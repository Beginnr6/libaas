import { ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { HttpClient } from '@angular/common/http';
import { UntypedFormControl, UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NzFormTooltipIcon } from 'ng-zorro-antd/form';
import { DatePipe } from '@angular/common';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})
export class EditItemComponent implements OnInit {
  dateFormat ='dd-MM-yyyy';
  validateForm!: UntypedFormGroup;
  captchaTooltipIcon: NzFormTooltipIcon = {
    type: 'info-circle',
    theme: 'twotone'
  };
  listOfData: [] = [];
  keys: [] = [];
  id:any;

  constructor(
    private fb: UntypedFormBuilder,
    private appService: AppService,
    private message: NzMessageService,
    private route: ActivatedRoute,
    private datePipe: DatePipe
    ) {}

  submitForm(): void {
    if (this.validateForm.valid) {
      console.log('submit', this.validateForm.value);
      this.appService.editItem(this.id,  this.validateForm.value)
      .subscribe(
        (res: any) => {
          this.message.create('success', res.message);
        },
        (error: any) => {
          this.message.create('error', error.error);
          console.log(error.error);
        }
      );
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }``
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() => this.validateForm.controls['checkPassword'].updateValueAndValidity());
  }

  confirmationValidator = (control: UntypedFormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.validateForm.controls['password'].value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  getCaptcha(e: MouseEvent): void {
    e.preventDefault();
  }



  ngOnInit(): void {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required, Validators.minLength(5)]],
      category: [null, [Validators.required, Validators.minLength(5)]],
      price: [null, [Validators.required]],
      discount: [null, [Validators.required]],
      description: [null, [Validators.required, Validators.minLength(5)]],
      created_on: [null, [Validators.required, Validators.minLength(5)]]
    });
     this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) {
      this.getItemDetails(this.id)
    }
  }

  getItemDetails(id:any){
    this.appService.getItemById(id)
    .subscribe(
      (res: any) => {
        let data:any = Object.keys(res);
        data.pop();
        data.shift();
        this.keys = data;
        this.listOfData = res;
        console.log(res);
        this.validateForm.patchValue(this.listOfData);
      },
      (error: any) => {
        this.message.create('error', error.error);
        console.log(error.error);
      }
    );
  }

}

