import { UtilityService } from './../../services/utility.service';
import { AppService } from './../../services/app.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { HttpClient } from '@angular/common/http';
import { UntypedFormControl, UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NzFormTooltipIcon } from 'ng-zorro-antd/form';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {
  dateFormat ='dd-MM-yyyy'
  validateForm!: UntypedFormGroup;
  captchaTooltipIcon: NzFormTooltipIcon = {
    type: 'info-circle',
    theme: 'twotone'
  };
  constructor(
    private fb: UntypedFormBuilder,
    private appService: AppService,
    private message: NzMessageService,
    private datePipe: DatePipe,
    private utilityService : UtilityService
    ) {}
  submitForm(): void {
    let createdon = this.utilityService.dateFormatter(this.validateForm.controls['created_on'].value);
    if (this.validateForm.valid) {
      Object.assign(this.validateForm.value, {created_on: createdon})
      this.appService.addItem(this.validateForm.value)
      .subscribe(
        (res: any) => {
          this.message.create('success', res.message);
        },
        (error: any) => {
          this.message.create('error', error.error);
          console.log(error.error);
        }
      );
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }``
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required, Validators.minLength(5)]],
      category: [null, [Validators.required, Validators.minLength(5)]],
      price: [null, [Validators.required]],
      discount: [null, [Validators.required]],
      description: [null, [Validators.required, Validators.minLength(5)]],
      created_on: [null, [Validators.required, Validators.minLength(5)]]
    });
  }
}
