import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {
  NzTableFilterFn,
  NzTableFilterList,
  NzTableSortFn,
  NzTableSortOrder,
} from 'ng-zorro-antd/table';
import { AppService } from '../services/app.service';
import { UtilityService } from '../services/utility.service';

interface DataItem {
  _id: string;
  name: string;
  category: string;
  price: string;
  discount: string;
  description: string;
  created_on: string;
}

interface ColumnItem {
  name: string;
  sortOrder: NzTableSortOrder | null;
  sortFn: NzTableSortFn<DataItem> | null;
  listOfFilter: NzTableFilterList;
  filterFn: NzTableFilterFn<DataItem> | null;
  filterMultiple: boolean;
  sortDirections: NzTableSortOrder[];
}
interface ColumnItem {
  name: string;
  sortOrder: NzTableSortOrder | null;
  sortFn: NzTableSortFn<DataItem> | null;
  listOfFilter: NzTableFilterList;
  filterFn: NzTableFilterFn<DataItem> | null;
  filterMultiple: boolean;
  sortDirections: NzTableSortOrder[];
}
@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
})
export class ItemComponent implements OnInit {
  public listOfData: DataItem[] = [];
  public keys: [] = [];
  dateFormat ='dd-MM-yyyy'
  validateForm!: UntypedFormGroup;
  constructor(
    private fb: UntypedFormBuilder,
    private appService: AppService,
    private message: NzMessageService,
    private utilityService : UtilityService) {}
  ngOnInit() {
    this.getItemDetails();
    this.initForm();
  }
  listOfColumns: ColumnItem[] = [
    {
      name: 'Item Name',
      sortOrder: 'descend',
      sortFn: (a: DataItem, b: DataItem) =>
        a.name.localeCompare(b.name),
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true,
    },
    {
      name: 'Category',
      sortOrder: 'descend',
      sortFn: (a: DataItem, b: DataItem) =>
        a.category.localeCompare(b.category),
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true,
    },
    {
      name: 'Price Per unit',
      sortOrder: 'descend',
      sortFn: (a: DataItem, b: DataItem) =>
        a.price.localeCompare(b.price),
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true,
    },
    {
      name: 'Discount',
      sortOrder: 'descend',
      sortFn: (a: DataItem, b: DataItem) =>
        a.discount.localeCompare(b.discount),
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true,
    },
    {
      name: 'Description ',
      sortOrder: 'descend',
      sortFn: (a: DataItem, b: DataItem) => a.description.localeCompare(b.description),
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true,
    },
    {
      name: 'Created on',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: DataItem, b: DataItem) => a.created_on.length - b.created_on.length,
      filterMultiple: false,
      listOfFilter: [
        { text: 'London', value: 'London' },
        { text: 'Sidney', value: 'Sidney' },
      ],
      filterFn: (created_on: string, item: DataItem) =>
        item.created_on.indexOf(created_on) !== -1,
    },
  ];

  getItemDetails() {
    this.appService.getItems().subscribe(
      (res: any) => {
        let data: any = Object.keys(res[0]);
        data.pop();
        data.shift();
        this.keys = data;
        this.listOfData = res;
      },
      (error: any) => {
        this.message.create('error', error.error);
        console.log(error.error);
      }
    );
  }
  delete(id: string) {
    console.log(id);
    this.appService.deleteItem(id).subscribe(
      (res: any) => {
        this.getItemDetails();
        this.message.create('success', res.message);
      },
      (error: any) => {
        this.message.create('error', error.error);
        console.log(error.error);
      }
    );
  }
  // model related
  isVisible = false;
  isOkLoading = false;

  showModal(): void {
    this.validateForm.reset();
    this.isVisible = true;
  }
  handleOk(): void {
    this.submitForm();
  }
  handleCancel(): void {
    this.isVisible = false;
  }

  submitForm(): void {
    this.isOkLoading = true;
    let createdon = this.utilityService.dateFormatter(this.validateForm.controls['created_on'].value);
    if (this.validateForm.valid) {
      Object.assign(this.validateForm.value, {created_on: createdon})
      this.appService.addCategory(this.validateForm.value)
      .subscribe(
        (res: any) => {
          this.isVisible = false;
          this.isOkLoading = false;
          this.message.create('success', res.message);
        },
        (error: any) => {
          this.message.create('error', error.error);
          console.log(error.error);
        }
      );
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  initForm(){
    this.validateForm = this.fb.group({
      category_name: [null, [Validators.required, Validators.minLength(5)]],
      description: [null, [Validators.required, Validators.minLength(5)]],
      created_on: [null, [Validators.required]]
    });
  }
}
