import { UtilityService } from './../../services/utility.service';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import {
  UntypedFormGroup,
  UntypedFormBuilder,
  Validators,
} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-edit-expense',
  templateUrl: './edit-expense.component.html',
  styleUrls: ['./edit-expense.component.css'],
})
export class EditExpenseComponent implements OnInit {
  validateForm!: UntypedFormGroup;
  listOfData: [] = [];
  keys: [] = [];
  id: any;

  constructor(
    private fb: UntypedFormBuilder,
    private appService: AppService,
    private message: NzMessageService,
    private route: ActivatedRoute,
    private utilityService: UtilityService
  ) {}

  submitForm(): void {
    let date = this.utilityService.dateFormatter(
      this.validateForm.controls['expense_date'].value
    );
    Object.assign(this.validateForm.value, { expense_date: date });
    if (this.validateForm.valid) {
      this.appService.editExpense(this.id, this.validateForm.value).subscribe(
        (res: any) => {
          this.message.create('success', res.message);
        },
        (error: any) => {
          this.message.create('error', error.error);
          console.log(error.error);
        }
      );
    } else {
      Object.values(this.validateForm.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
    ``;
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      category: [null, [Validators.required, Validators.minLength(5)]],
      customer: [null, [Validators.required, Validators.minLength(5)]],
      expense_date: [null, [Validators.required, Validators.minLength(6)]],
      notes: [null, [Validators.required, Validators.minLength(5)]],
      amount: [null, [Validators.required, Validators.minLength(5)]],
      status: [null, [Validators.required, Validators.minLength(5)]],
    });
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) {
      this.getInvoiceDetails(this.id);
    }
  }

  getInvoiceDetails(id: any) {
    this.appService.getExpenseById(id).subscribe(
      (res: any) => {
        let data: any = Object.keys(res);
        data.pop();
        data.shift();
        this.keys = data;
        this.listOfData = res;
        console.log(res);
        this.validateForm.patchValue(this.listOfData);
      },
      (error: any) => {
        this.message.create('error', error.error);
        console.log(error.error);
      }
    );
  }
}
