import { AppService } from 'src/app/services/app.service';
import { UtilityService } from './../../services/utility.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { HttpClient } from '@angular/common/http';
import {
  UntypedFormControl,
  UntypedFormGroup,
  UntypedFormBuilder,
  Validators,
} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NzFormTooltipIcon } from 'ng-zorro-antd/form';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-expense',
  templateUrl: './add-expense.component.html',
  styleUrls: ['./add-expense.component.css'],
})
export class AddExpenseComponent implements OnInit {
  dateFormat = 'dd-MM-yyyy';
  validateForm!: UntypedFormGroup;

  constructor(
    private fb: UntypedFormBuilder,
    private http: HttpClient,
    private message: NzMessageService,
    private appService: AppService,
    private utilityService: UtilityService
  ) {}

  submitForm(): void {
    if (this.validateForm.valid) {
      let date = this.utilityService.dateFormatter(
        this.validateForm.controls['expense_date'].value
      );
      Object.assign(this.validateForm.value, { expense_date: date });
      this.appService.addExpense(this.validateForm.value).subscribe(
        (res: any) => {
          this.message.create('success', res.message);
        },
        (error: any) => {
          this.message.create('error', error.error);
          console.log(error.error);
        }
      );
    } else {
      Object.values(this.validateForm.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      category: [null, [Validators.required, Validators.minLength(5)]],
      customer: [null, [Validators.required, Validators.minLength(5)]],
      expense_date: [null, [Validators.required]],
      notes: [null, [Validators.required]],
      amount: [null, [Validators.required, Validators.minLength(5)]],
      status: [null, [Validators.required, Validators.minLength(5)]],
    });
  }
}
