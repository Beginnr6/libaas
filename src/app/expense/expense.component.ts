import { NzMessageService } from 'ng-zorro-antd/message';
import { Component, OnInit } from '@angular/core';
import {
  NzTableFilterFn,
  NzTableFilterList,
  NzTableSortFn,
  NzTableSortOrder,
} from 'ng-zorro-antd/table';
import { AppService } from '../services/app.service';

interface DataItem {
  _id: string;
  category: string;
  customer: string;
  expense_date: string;
  notes: string;
  amount: string;
  status: string;
}

interface ColumnItem {
  name: string;
  sortOrder: NzTableSortOrder | null;
  sortFn: NzTableSortFn<DataItem> | null;
  listOfFilter: NzTableFilterList;
  filterFn: NzTableFilterFn<DataItem> | null;
  filterMultiple: boolean;
  sortDirections: NzTableSortOrder[];
}
interface ColumnItem {
  name: string;
  sortOrder: NzTableSortOrder | null;
  sortFn: NzTableSortFn<DataItem> | null;
  listOfFilter: NzTableFilterList;
  filterFn: NzTableFilterFn<DataItem> | null;
  filterMultiple: boolean;
  sortDirections: NzTableSortOrder[];
}
@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.css']
})
export class ExpenseComponent implements OnInit {
  public listOfData: DataItem[] = [];
  public keys: [] = [];
  constructor(private appService: AppService, private message: NzMessageService) {}
  ngOnInit() {
    this.getExpenseDetails();
  }
  listOfColumns: ColumnItem[] = [
    {
      name: 'Cagetory',
      sortOrder: 'descend',
      sortFn: (a: DataItem, b: DataItem) => a.category.localeCompare(b.category),
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true,
    },
    {
      name: 'Customer',
      sortOrder: 'descend',
      sortFn: (a: DataItem, b: DataItem) => a.customer.localeCompare(b.customer),
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true,
    },
    {
      name: 'Expense Date',
      sortOrder: 'descend',
      sortFn: (a: DataItem, b: DataItem) => a.expense_date.localeCompare(b.expense_date),
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true,
    },
    {
      name: 'notes',
      sortOrder: 'descend',
      sortFn: (a: DataItem, b: DataItem) => a.notes.localeCompare(b.notes),
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true,
    },
    {
      name: 'Amount',
      sortOrder: 'descend',
      sortFn: (a: DataItem, b: DataItem) => a.amount.localeCompare(b.amount),
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true,
    },
    {
      name: 'Status',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: DataItem, b: DataItem) => a.status.length - b.status.length,
      filterMultiple: false,
      listOfFilter: [
        { text: 'London', value: 'London' },
        { text: 'Sidney', value: 'Sidney' },
      ],
      filterFn: (status: string, item: DataItem) =>
        item.status.indexOf(status) !== -1,
    }
  ];

  getExpenseDetails() {
    this.appService.getExpenses().subscribe(
      (res: any) => {
        let data: any = Object.keys(res[0]);
        data.pop();
        data.shift();
        this.keys = data;
        this.listOfData = res;
      },
      (error: any) => {
        this.message.create('error', error.error);
        console.log(error.error);
      }
    );
  }
  delete(id: string) {
    console.log(id);
    this.appService.deleteExpense(id).subscribe(
      (res: any) => {
        this.getExpenseDetails();
        this.message.create('success', res.message);
      },
      (error: any) => {
        this.message.create('error', error.error);
        console.log(error.error);
      }
    );
  }
}
