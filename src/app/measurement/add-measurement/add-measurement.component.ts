import { AppService } from './../../services/app.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { HttpClient } from '@angular/common/http';
import {
  UntypedFormControl,
  UntypedFormGroup,
  UntypedFormBuilder,
  Validators,
} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NzFormTooltipIcon } from 'ng-zorro-antd/form';

@Component({
  selector: 'app-add-measurement',
  templateUrl: './add-measurement.component.html',
  styleUrls: ['./add-measurement.component.css'],
})
export class AddMeasurementComponent implements OnInit {
  validateForm!: UntypedFormGroup;
  topForm!: UntypedFormGroup;
  neckForm!: UntypedFormGroup;
  sleavesForm!: UntypedFormGroup;
  chestForm!: UntypedFormGroup;
  bottomForm!: UntypedFormGroup;
  captchaTooltipIcon: NzFormTooltipIcon = {
    type: 'info-circle',
    theme: 'twotone',
  };
  rowGutter:any = [24];
  constructor(
    private fb: UntypedFormBuilder,
    private appService: AppService,
    private message: NzMessageService
  ) {}

  submitForm(data:any): void {
    // if (this.validateForm.valid) {
      // console.log('submit', this.validateForm.value);
      this.appService.addMeasurement(data).subscribe(
        (res: any) => {
          this.message.create('success', res.message);
        },
        (error: any) => {
          this.message.create('error', error.error);
          console.log(error.error);
        }
      );
    // } else {
    //   Object.values(this.validateForm.controls).forEach((control) => {
    //     if (control.invalid) {
    //       control.markAsDirty();
    //       control.updateValueAndValidity({ onlySelf: true });
    //     }
    //   });
    // }
    // ``;
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() =>
      this.validateForm.controls['checkPassword'].updateValueAndValidity()
    );
  }

  confirmationValidator = (
    control: UntypedFormControl
  ): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.validateForm.controls['password'].value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  getCaptcha(e: MouseEvent): void {
    e.preventDefault();
  }

  ngOnInit(): void {
    // top
    this.topForm = this.fb.group({
      kurti_type: [null, [Validators.required, Validators.minLength(1)]],
      kurti_length: [null, [Validators.required, Validators.minLength(1)]],
      chest_length: [null, [Validators.required, Validators.minLength(1)]],
      waist_length: [null, [Validators.required, Validators.minLength(1)]],
      heap_length: [null, [Validators.required, Validators.minLength(1)]],
    });
    // Neck:
    this.neckForm = this.fb.group({
      neck_type: [null, [Validators.required, Validators.minLength(1)]],
      front_neck_length: [null, [Validators.required, Validators.minLength(1)]],
      back_neck_length: [null, [Validators.required, Validators.minLength(1)]],
    });
    // Sleaves:
    this.sleavesForm = this.fb.group({
      sleaves_type: [null, [Validators.required, Validators.minLength(1)]],
      sleaves_length: [null, [Validators.required, Validators.minLength(1)]],
      arm_holes: [null, [Validators.required, Validators.minLength(1)]],
      arm_round_left: [null, [Validators.required, Validators.minLength(1)]],
      arm_round_right: [null, [Validators.required, Validators.minLength(1)]],
      sleaves_flare: [null, [Validators.required, Validators.minLength(1)]],
    });
    // Chest:
    this.chestForm= this.fb.group({
      upper_chest: [null, [Validators.required, Validators.minLength(1)]],
      lower_chest: [null, [Validators.required, Validators.minLength(1)]],
      waist: [null, [Validators.required, Validators.minLength(1)]],
      heap: [null, [Validators.required, Validators.minLength(1)]],
      seat: [null, [Validators.required, Validators.minLength(1)]],
      other: [null, [Validators.required, Validators.minLength(1)]],
    });
      // Bottom
      this.bottomForm= this.fb.group({
        bottom_length: [null, [Validators.required, Validators.minLength(1)]],
        bottom_waist: [null, [Validators.required, Validators.minLength(1)]],
        bottom_heap: [null, [Validators.required, Validators.minLength(1)]],
        bottom_seat: [null, [Validators.required, Validators.minLength(1)]],
        bottom_theigh: [null, [Validators.required, Validators.minLength(1)]],
        bottom_knee: [null, [Validators.required, Validators.minLength(1)]],
        bottom_calf: [null, [Validators.required, Validators.minLength(1)]],
        bottom_ancle: [null, [Validators.required, Validators.minLength(1)]],
        bottom_bottom: [null, [Validators.required, Validators.minLength(1)]],
        bottom_other: [null, [Validators.required, Validators.minLength(1)]]
      });
  }

  current = 0;

  index: any = 'top';

  pre(): void {
    this.current -= 1;
    this.changeContent();
  }

  next(): void {
    this.current += 1;
    this.changeContent();
  }

  done(): void {
    console.log('done');
  }

  changeContent(): void {
    switch (this.current) {
      case 0: {
        this.index = 'top';
        break;
      }
      case 1: {
        this.index = 'neck';
        break;
      }
      case 2: {
        this.index = 'sleaves';
        break;
      }
      case 3: {
        this.index = 'chest';
        break;
      }
      case 4: {
        this.index = 'bottom';
        break;
      }
      default: {
        this.index = null;
      }
    }
  }
  topFormSave():void{

  }
  neckFormSave():void{

  }
sleavesFormSave():void{

  }
  finalData:any;
  chestFormSave():void{
    this.finalData ={
      ...this.topForm.value,
      ...this.neckForm.value,
      ...this.sleavesForm.value,
      ...this.chestForm.value
    };

    console.log(this.finalData);
    this.done();
    this.submitForm(this.finalData);
  }
  bottonFormSave():void{
    this.finalData ={
      ...this.topForm.value,
      ...this.neckForm.value,
      ...this.sleavesForm.value,
      ...this.chestForm.value,
      ...this.bottomForm.value
    };

    console.log(this.finalData);
    this.done();
    this.submitForm(this.finalData);
  }
}
