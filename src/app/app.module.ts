import { EditMeasurementComponent } from './measurement/edit-measurement/edit-measurement.component';
import { AddMeasurementComponent } from './measurement/add-measurement/add-measurement.component';
import { AddItemComponent } from './item/add-item/add-item.component';
import { ItemComponent } from './item/item.component';
import { EditInvoiceComponent } from './invoice/edit-invoice/edit-invoice.component';
import { AddInvoiceComponent } from './invoice/add-invoice/add-invoice.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { EditCustomerComponent } from './customer/edit-customer/edit-customer.component';
import { AddCustomerComponent } from './customer/add-customer/add-customer.component';
import { CustomerComponent } from './customer/customer.component';
import { AuthGuard } from './services/auth/auth.guard';
import { AuthService } from './services/auth/auth.service';
import { AuthInterceptor } from './services/auth/auth.interceptor';
import { AppService } from './services/app.service';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { FooterLayoutComponent } from './layout/footer-layout/footer-layout.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HeaderComponent } from './layout/header/header.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { SignupComponent } from './common/signup/signup.component';
import { LoginComponent } from './common/login/login.component';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DatePipe, registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import {  FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IconsProviderModule } from './icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NZ_ICONS } from 'ng-zorro-antd/icon';
import { NZ_I18N, en_US } from 'ng-zorro-antd/i18n';
import { IconDefinition } from '@ant-design/icons-angular';
import * as AllIcons from '@ant-design/icons-angular/icons';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { UtilityService } from './services/utility.service';
import { PageNotFoundComponent } from './layout/page-not-found/page-not-found.component';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { EditItemComponent } from './item/edit-item/edit-item.component';
import { ExpenseComponent } from './expense/expense.component';
import { AddExpenseComponent } from './expense/add-expense/add-expense.component';
import { EditExpenseComponent } from './expense/edit-expense/edit-expense.component';
import { CategoryComponent } from './category/category.component';
import { AddCategoryComponent } from './category/add-category/add-category.component';
import { EditCategoryComponent } from './category/edit-category/edit-category.component';
import { MeasurementComponent } from './measurement/measurement.component';
import { NzStepsModule } from 'ng-zorro-antd/steps';

registerLocaleData(en);

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(key => antDesignIcons[key])



@NgModule({
  declarations: [

    AppComponent,
    LoginComponent,
    SignupComponent,
    SidebarComponent,
    HeaderComponent,
    FooterComponent,
    FooterLayoutComponent,
    MainLayoutComponent,
    CustomerComponent,
    AddCustomerComponent,
    EditCustomerComponent,
    InvoiceComponent,
    AddInvoiceComponent,
    EditInvoiceComponent,
    ItemComponent,
    AddItemComponent,
    EditItemComponent,
    ExpenseComponent,
    AddExpenseComponent,
    EditExpenseComponent,
    CategoryComponent,
    AddCategoryComponent,
    EditCategoryComponent,
    PageNotFoundComponent,
    MeasurementComponent,
    AddMeasurementComponent,
    EditMeasurementComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    IconsProviderModule,
    NzGridModule,
    NzLayoutModule,
    NzMenuModule,
    NzAvatarModule,
    NzDropDownModule,
    NzSpaceModule,
    FormsModule,
    NzInputModule,
    ReactiveFormsModule,
    NzButtonModule,
    NzMenuModule,
    NzCheckboxModule,
    NzSelectModule,
    NzSwitchModule,
    NzUploadModule,
    NzImageModule,
    NzListModule,
    NzPopoverModule,
    NzTableModule,
    NzTabsModule,
    NzToolTipModule,
    NzBadgeModule,
    NzInputNumberModule,
    NzRadioModule,
    NzFormModule,
    NzCardModule,
    NzMessageModule,
    ScrollingModule,
    DragDropModule,
    NzDividerModule,
    NzDatePickerModule,
    NzModalModule,
    NzStepsModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    { provide: NZ_ICONS, useValue: icons },
    DatePipe,
    AppService,
    UtilityService ,
    AuthService,
    AuthGuard,
    {
      provide : HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi   : true,
    },],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }



// registerLocaleData(en);

// const antDesignIcons = AllIcons as {
//   [key: string]: IconDefinition;
// };
// const icons: IconDefinition[] = Object.keys(antDesignIcons).map(key => antDesignIcons[key])

// @NgModule({
//   declarations: [
//     AppComponent,
//     LoginComponent,
//     SignupComponent
//   ],
//   // imports: [
//   //   BrowserModule,
//   //   AppRoutingModule,
//   //   FormsModule,
//   //   HttpClientModule,
//   //   BrowserAnimationsModule,
//   //   IconsProviderModule,
//   //   NzLayoutModule,
//   //   NzMenuModule
//   // ],
//   // providers: [
//   //   { provide: NZ_I18N, useValue: en_US }
//   // ],
//   imports: [
//     BrowserModule,
//     AppRoutingModule,
//     HttpClientModule,
//     BrowserAnimationsModule,
//     IconsProviderModule,
//     NzGridModule,
//     NzLayoutModule,
//     NzMenuModule,
//     NzAvatarModule,
//     NzDropDownModule,
//     NzSpaceModule,
//     FormsModule,
//     NzInputModule,
//     ReactiveFormsModule,
//     NzButtonModule,
//     NzMenuModule,
//     NzCheckboxModule,
//     NzSelectModule,
//     NzSwitchModule,
//     NzUploadModule,
//     NzImageModule,
//     NzListModule,
//     NzPopoverModule,
//     NzTableModule,
//     NzTabsModule,
//     NzToolTipModule,
//     NzBadgeModule,
//     NzInputNumberModule,
//     NzRadioModule,
//     NzFormModule,
//     NzCardModule,
//     NzMessageModule,
//     ScrollingModule,
//     DragDropModule,
//     NzDividerModule,
//     NzDatePickerModule,
//     NzModalModule
//   ],
//   providers: [
//     { provide: NZ_I18N, useValue: en_US },
//     { provide: NZ_ICONS, useValue: icons },
//     DatePipe,
//     AppService,
//     UtilityService ,
//     AuthService,
//     AuthGuard,
//     {
//       provide : HTTP_INTERCEPTORS,
//       useClass: AuthInterceptor,
//       multi   : true,
//     },],
//   bootstrap: [AppComponent],
//   schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
// })
// export class AppModule { }
