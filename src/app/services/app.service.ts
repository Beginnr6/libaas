import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  endpoint = 'http://localhost:3000/api';

  constructor(private http: HttpClient) {}

  // Api methods
  public get(url: string, options?: any) {
    return this.http.get(url, options);
  }
  public post(url: string, data: any, options?: any) {
    return this.http.post(url, data, options);
  }
  public put(url: string, data: any, options?: any) {
    return this.http.put(url, data, options);
  }
  public delete(url: string, options?: any) {
    return this.http.delete(url, options);
  }

  // Customer API
  getCustomers() {
    return this.http.get(this.endpoint + '/customers');
  }
  getCustomerById(id: any) {
    return this.http.get(this.endpoint + '/customers/' + id);
  }
  addCustomer(data: any) {
    return this.http.post(this.endpoint + '/customers', data);
  }
  editCustomer(id: any, data: any) {
    return this.http.put(this.endpoint + '/customers/' + id, data);
  }
  deleteCustomer(id: any) {
    return this.http.delete(this.endpoint + '/customers/' + id);
  }
 // Measurement API
  addMeasurement(data: any) {
    return this.http.post(this.endpoint + '/measurement', data);
  }
  // Invoice API
  getInvoices() {
    return this.http.get(this.endpoint + '/invoices');
  }
  getInvoiceById(id: any) {
    return this.http.get(this.endpoint + '/invoices/' + id);
  }
  addInvoice(data: any) {
    return this.http.post(this.endpoint + '/invoices', data);
  }
  editInvoice(id: any, data: any) {
    return this.http.put(this.endpoint + '/invoices/' + id, data);
  }
  deleteInvoice(id: any) {
    return this.http.delete(this.endpoint + '/invoices/' + id);
  }
  // Estimate API
  getEstimates() {
    return this.http.get(this.endpoint + '/estimates');
  }
  getEstimateById(id: any) {
    return this.http.get(this.endpoint + '/estimates/' + id);
  }
  addEstimate(data: any) {
    return this.http.post(this.endpoint + '/estimates', data);
  }
  editEstimate(id: any, data: any) {
    return this.http.put(this.endpoint + '/estimates/' + id, data);
  }
  deleteEstimate(id: any) {
    return this.http.delete(this.endpoint + '/estimates/' + id);
  }
  // Item API
  getItems() {
    return this.http.get(this.endpoint + '/items');
  }
  getItemById(id: any) {
    return this.http.get(this.endpoint + '/items/' + id);
  }
  addItem(data: any) {
    return this.http.post(this.endpoint + '/items', data);
  }
  editItem(id: any, data: any) {
    return this.http.put(this.endpoint + '/items/' + id, data);
  }
  deleteItem(id: any) {
    return this.http.delete(this.endpoint + '/items/' + id);
  }
  // Expense API
  getExpenses() {
    return this.http.get(this.endpoint + '/expenses');
  }
  getExpenseById(id: any) {
    return this.http.get(this.endpoint + '/expenses/' + id);
  }
  addExpense(data: any) {
    return this.http.post(this.endpoint + '/expenses', data);
  }
  editExpense(id: any, data: any) {
    return this.http.put(this.endpoint + '/expenses/' + id, data);
  }
  deleteExpense(id: any) {
    return this.http.delete(this.endpoint + '/expenses/' + id);
  }
  // Category API
  getCategories() {
    return this.http.get(this.endpoint + '/categories');
  }
  getCategoryById(id: any) {
    return this.http.get(this.endpoint + '/categories/' + id);
  }
  addCategory(data: any) {
    return this.http.post(this.endpoint + '/categories', data);
  }
  editCategory(id: any, data: any) {
    return this.http.put(this.endpoint + '/categories/' + id, data);
  }
  deleteCategory(id: any) {
    return this.http.delete(this.endpoint + '/categories/' + id);
  }
}
