import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

constructor(private datePipe: DatePipe) { }

dateFormatter(enterDate:any){
  let date =null;
if (enterDate) {
 date= this.datePipe.transform(enterDate, "dd-MM-yyyy");
}
return date
}
}
