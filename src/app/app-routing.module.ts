import { MeasurementComponent } from './measurement/measurement.component';
import { AddMeasurementComponent } from './measurement/add-measurement/add-measurement.component';
import { EditMeasurementComponent } from './measurement/edit-measurement/edit-measurement.component';
import { EditExpenseComponent } from './expense/edit-expense/edit-expense.component';
import { AddExpenseComponent } from './expense/add-expense/add-expense.component';
import { ExpenseComponent } from './expense/expense.component';
import { EditCategoryComponent } from './category/edit-category/edit-category.component';
import { AddCategoryComponent } from './category/add-category/add-category.component';
import { CategoryComponent } from './category/category.component';
import { EditItemComponent } from './item/edit-item/edit-item.component';
import { AddItemComponent } from './item/add-item/add-item.component';
import { ItemComponent } from './item/item.component';
import { EditInvoiceComponent } from './invoice/edit-invoice/edit-invoice.component';
import { AddInvoiceComponent } from './invoice/add-invoice/add-invoice.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { EditCustomerComponent } from './customer/edit-customer/edit-customer.component';
import { AddCustomerComponent } from './customer/add-customer/add-customer.component';
import { CustomerComponent } from './customer/customer.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { PageNotFoundComponent } from './layout/page-not-found/page-not-found.component';
import { SignupComponent } from './common/signup/signup.component';
import { LoginComponent } from './common/login/login.component';
import { AuthGuard } from './services/auth/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// const routes: Routes = [
//   { path: '', pathMatch: 'full', redirectTo: '/welcome' },
//   { path: 'welcome', loadChildren: () => import('./pages/welcome/welcome.module').then(m => m.WelcomeModule) }
// ];

const routes: Routes = [
  // {
  //   path: 'login',
  //   component: FooterLayoutComponent,
  //   children: [
  //     { path: '', component: LoginComponent },
  //   ]
  // }
  // { path: 'welcome', loadChildren: () => import('./pages/welcome/welcome.module').then(m => m.WelcomeModule) },

   // App routes goes here here
   {
    path: '',
    component: MainLayoutComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
      { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]   },
      { path: 'customer', component: CustomerComponent, canActivate: [AuthGuard] },
      { path: 'add-customer', component: AddCustomerComponent, canActivate: [AuthGuard]  },
      { path: 'edit-customer/:id', component: EditCustomerComponent, canActivate: [AuthGuard]  },
      { path: 'measurement', component: MeasurementComponent, canActivate: [AuthGuard] },
      { path: 'add-measurement', component: AddMeasurementComponent, },
      { path: 'edit-measurement/:id', component: EditMeasurementComponent, canActivate: [AuthGuard]  },
      { path: 'invoice', component: InvoiceComponent, canActivate: [AuthGuard]  },
      { path: 'add-invoice', component: AddInvoiceComponent, canActivate: [AuthGuard]  },
      { path: 'edit-invoice/:id', component: EditInvoiceComponent, canActivate: [AuthGuard]  },
      // { path: 'estimate', component: EstimateComponent, canActivate: [AuthGuard]  },
      // { path: 'add-estimate', component: AddEstimateComponent, canActivate: [AuthGuard]  },
      // { path: 'edit-estimate/:id', component: EditEstimateComponent, canActivate: [AuthGuard]  },
      { path: 'item', component: ItemComponent, canActivate: [AuthGuard]  },
      { path: 'add-item', component: AddItemComponent, canActivate: [AuthGuard]  },
      { path: 'edit-item/:id', component: EditItemComponent, canActivate: [AuthGuard]  },
      { path: 'category', component: CategoryComponent, canActivate: [AuthGuard]  },
      { path: 'add-category', component: AddCategoryComponent, canActivate: [AuthGuard]  },
      { path: 'edit-category/:id', component: EditCategoryComponent, canActivate: [AuthGuard]  },
      { path: 'expense', component: ExpenseComponent, canActivate: [AuthGuard]  },
      { path: 'add-expense', component: AddExpenseComponent, canActivate: [AuthGuard]  },
      { path: 'edit-expense/:id', component: EditExpenseComponent, canActivate: [AuthGuard]  },
    ]
},

//no layout routes
{ path: 'login', component: LoginComponent},
{ path: 'signup', component: SignupComponent },
// otherwise redirect to home
{ path: '**', pathMatch: 'full',
component: PageNotFoundComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
